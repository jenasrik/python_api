The Python API is designed to run with the SEL-AFT framework. 

If you are unsure if you have the AFT installed on your device, go to All Programs -> SEL Applications. You should see a few 
AFT (Automated Functional Testing) icons as well as a test manager.

If you do not have this, then use the provided installer in this folder (AFT-R115-V0-B99.msi). If the installer does not
work or is out of date, you can also access the folder by going to O:PC-Inst\SelfServe\RDTools\AFT and then selecting 
the latest r version of the AFT. 

For information on the AFT, visit https://confluence.metro.ad.selinc.com/display/AFT/Getting+started. Visit this page if you
run into any problems with the AFT or its setup. 