@author Jena Srikanth

This is the Python API for the SAMS application. Through this, users will be able to load and execute existing test files
on the SAMS.

This class is not intended for creating tests. Users may generate a test file by using the SAMS GUI application, which
is found under the sams_sw repository of the SEL-SAMs. They may also use the Java installer to install the 
Java_Application_v2, which will launch the GUI. 

This class is intended to be compatible with the SEL-AFT framework. If you don't have the AFT already installed on your device,
check the AFT folder of this repository for more information.

For more information on this API, visit https://confluence.metro.ad.selinc.com/display/PSTT/Python+API. 